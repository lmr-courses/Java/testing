-- CREATE DATABASE testing;

-- DROP TABLE IF EXISTS movies;
CREATE TABLE IF NOT EXISTS movies (
    id SERIAL NOT NULL,
    name VARCHAR(50) NOT NULL,
    minutes INTEGER NOT NULL,
    genre VARCHAR(50) NOT NULL,
    CONSTRAINT movies_pk PRIMARY KEY (id)
);

INSERT INTO movies(name, minutes, genre) VALUES
    ('Dark Knight', 152, 'ACTION'),
    ('Memento', 113, 'THRILLER'),
    ('Theres Something About Mary', 119, 'COMEDY'),
    ('Super 8', 112, 'THRILLER'),
    ('Scream', 111, 'HORROR'),
    ('Home Alone', 103, 'COMEDY'),
    ('Matrix', 136, 'ACTION');

-- SELECT * FROM movies;