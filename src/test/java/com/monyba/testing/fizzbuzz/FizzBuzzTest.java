package com.monyba.testing.fizzbuzz;

import org.junit.Test;

import static org.junit.Assert.*;

public class FizzBuzzTest {

    @Test
    public void return_fizz_when_number_is_divisible_by_3(){
        assertEquals(FizzBuzz.fizzBuzz(3),"Fizz");
    }

    @Test
    public void return_buzz_when_number_is_divisible_by_5(){
        assertEquals(FizzBuzz.fizzBuzz(5),"Buzz");
    }

    @Test
    public void return_fizzbuzz_when_number_is_divisible_by_3_and_5(){
        assertEquals(FizzBuzz.fizzBuzz(15),"FizzBuzz");
    }

    @Test
    public void return_the_same_number_when_is_not_3_or_5(){
        assertEquals(FizzBuzz.fizzBuzz(2),"2");
    }
}