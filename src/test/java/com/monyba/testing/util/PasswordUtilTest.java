package com.monyba.testing.util;

import org.junit.Test;

import static com.monyba.testing.util.PasswordUtil.SecurityLevel.*;
import static org.junit.Assert.*;

public class PasswordUtilTest {

    @Test
    public void weak_when_has_less_than_8_letters(){
        assertEquals(PasswordUtil.assessPassword("123aa!"), WEAK);
    }

    @Test
    public void weak_when_has_only_letters(){
        assertEquals(PasswordUtil.assessPassword("abcdefghijk"), WEAK);
    }

    @Test
    public void medium_when_has_letters_and_numbers(){
        assertEquals(PasswordUtil.assessPassword("abcde12345"), MEDIUM);
    }

    @Test
    public void strong_when_has_letters_numbers_symbols(){
        assertEquals(PasswordUtil.assessPassword("abcd1234!"), STRONG);
    }

}