package com.monyba.testing.discounts;

import org.junit.Test;

import static org.junit.Assert.*;

public class PriceCalculatorShould {
    @Test
    public void total_zero_when_there_are_prices(){
        PriceCalculator priceCalculator = new PriceCalculator();
        double EPSILON = Math.ulp(1.0);
        assertEquals(0.0,priceCalculator.getTotal(),EPSILON);
    }

    @Test
    public void total_is_the_sum_of_prices(){
        PriceCalculator priceCalculator = new PriceCalculator();
        priceCalculator.addPrice(10.2);
        priceCalculator.addPrice(15.5);
        double EPSILON = Math.ulp(1.0);
        assertEquals(25.7,priceCalculator.getTotal(),EPSILON);
    }

    @Test
    public void apply_discount_to_prices(){
        PriceCalculator priceCalculator = new PriceCalculator();
        priceCalculator.addPrice(12.5);
        priceCalculator.addPrice(17.5);

        priceCalculator.setDiscountInPercent(25);

        double EPSILON = Math.ulp(1.0);
        assertEquals(22.5, priceCalculator.getTotal(), EPSILON);
    }

}