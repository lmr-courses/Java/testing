package com.monyba.testing.movies.service;

import com.monyba.testing.movies.data.MovieRepository;
import com.monyba.testing.movies.model.Genre;
import com.monyba.testing.movies.model.Movie;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.experimental.runners.Enclosed;
import org.junit.rules.TestName;
import org.junit.runner.RunWith;
import org.mockito.Mockito;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

import static org.junit.Assert.*;

@RunWith(Enclosed.class)
public class MovieServiceShould {

  public static MovieService getMovieService(){
    MovieRepository movieRepository = Mockito.mock(MovieRepository.class);

    Mockito.when(movieRepository.findAll()).thenReturn(
            Arrays.asList(
                    new Movie(1, "Dark Knight", 152, Genre.ACTION),
                    new Movie(2, "Memento", 113, Genre.THRILLER),
                    new Movie(3, "There's Something About Mary", 119, Genre.COMEDY),
                    new Movie(4, "Super 8", 112, Genre.THRILLER),
                    new Movie(5, "Scream", 111, Genre.HORROR),
                    new Movie(6, "Home Alone", 103, Genre.COMEDY),
                    new Movie(7, "Matrix", 136, Genre.ACTION)
            )
    );

    Mockito.when(movieRepository.findById(Mockito.anyLong())).thenReturn(
            new Movie(5, "Scream", 111, Genre.HORROR)
    );

    return new MovieService(movieRepository);
  }


  public static List<Integer> getMovieIds(Collection<Movie> movies) {
    return movies.stream().map(Movie::getId).collect(Collectors.toList());
  }


  public static class General{
    private MovieService movieService;

    @Before
    public void setUp() {
      movieService = getMovieService();
    }

    @Test
    public void return_movies_by_genre() {
      Collection<Movie> movies = movieService.findMoviesByGenre(Genre.COMEDY);
      assertEquals(Arrays.asList(3,6), getMovieIds(movies));
    }

    @Test
    public void return_movies_by_length() {
      Collection<Movie> movies = movieService.findMoviesByLength(119);
      assertEquals(Arrays.asList(2, 3, 4, 5, 6), getMovieIds(movies));
    }

    @Test
    public void return_movies_by_name() {
      Collection<Movie> movies = movieService.findMoviesByName("screa");
      assertEquals(Arrays.asList(5), getMovieIds(movies));
    }
  }

  public static class SearchByTemplateMethod {
    private MovieService movieService;

    @Before
    public void setUp() {
      movieService = getMovieService();
    }

    @Test
    public void whenUsingId() {
      Collection<Movie> movies = movieService.findMoviesByTemplate(new Movie(5, null, null, Genre.ACTION));
      assertEquals(Collections.singletonList(5), getMovieIds(movies));
    }

    @Test
    public void whenUsingNegativeMinutes() {
      assertThrows(IllegalArgumentException.class,() -> movieService.findMoviesByTemplate(new Movie(null, null, -15, Genre.ACTION)));
    }

    @Test
    public void whenUsingGenreAndMinutes() {
      Collection<Movie> movies = movieService.findMoviesByTemplate(new Movie(null, null, 180, Genre.ACTION));
      assertEquals(Arrays.asList(1, 7), getMovieIds(movies));
    }

    @Test
    public void whenUsingNameAndMinutes() {
      Collection<Movie> movies = movieService.findMoviesByTemplate(new Movie(null, "Matrix", 136, null));
      assertEquals(Arrays.asList(7), getMovieIds(movies));
    }

  }


}
