package com.monyba.testing.movies.data;

import com.monyba.testing.movies.model.Genre;
import com.monyba.testing.movies.model.Movie;
import com.monyba.testing.movies.service.MovieService;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.core.io.ClassPathResource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.jdbc.datasource.init.ScriptUtils;

import javax.sql.DataSource;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

public class MovieRepositoryIntegrationTest {
  DataSource dataSource;
  MovieRepositoryJdbc movieRepositoryJdbc;

  @Before
  public void setup() throws SQLException {
    // This is executed before each test

    // Database In Memory
    dataSource = new DriverManagerDataSource("jdbc:h2:mem:testing;MODE=PostgreSQL","sa","sa");
    ScriptUtils.executeSqlScript(dataSource.getConnection(), new ClassPathResource("sql-scripts/test-data.sql"));

    // Database In Disk
    // dataSource = new DriverManagerDataSource("jdbc:postgresql://localhost:5432/testing","test","test");

    JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
    movieRepositoryJdbc = new MovieRepositoryJdbc(jdbcTemplate);
  }

  @Test
  public void load_all_movies() {
    Collection<Movie> movies = movieRepositoryJdbc.findAll();
    Collection<Movie> expecteds = Arrays.asList(
            new Movie(1, "Dark Knight", 152, Genre.ACTION),
            new Movie(2, "Memento", 113, Genre.THRILLER),
            new Movie(3, "Theres Something About Mary", 119, Genre.COMEDY),
            new Movie(4, "Super 8", 112, Genre.THRILLER),
            new Movie(5, "Scream", 111, Genre.HORROR),
            new Movie(6, "Home Alone", 103, Genre.COMEDY),
            new Movie(7, "Matrix", 136, Genre.ACTION)
    );
    Assert.assertArrayEquals(expecteds.toArray(), movies.toArray());
  }

  @Test
  public void load_movie_by_id() {
    Movie movie = movieRepositoryJdbc.findById(2);
    Assert.assertEquals(new Movie(2, "Memento", 113, Genre.THRILLER), movie);
  }

  @Test
  public void insert_a_movie() {
    Movie expected = new Movie("El Naufrago", 120, Genre.DRAMA);
    movieRepositoryJdbc.saveOrUpdate(expected);
    Movie movie = movieRepositoryJdbc.findById(8);
    expected.setId(8);
    Assert.assertEquals(expected, movie);
  }


  @After
  public void tearDown() throws Exception {
    // This is executed after each test
    final Statement statement = dataSource.getConnection().createStatement();
    statement.execute("drop all objects delete files");
  }
}