package com.monyba.testing.payments;

public interface PaymentGateway {
    PaymentResponse requestPayment(PaymentRequest request);
}
