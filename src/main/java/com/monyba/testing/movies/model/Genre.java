package com.monyba.testing.movies.model;

public enum Genre {
    ACTION, COMEDY, DRAMA, HORROR, THRILLER
}
