package com.monyba.testing.movies.data;

import com.monyba.testing.movies.model.Genre;
import com.monyba.testing.movies.model.Movie;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;

public class MovieRepositoryJdbc implements MovieRepository {

  private final JdbcTemplate jdbcTemplate;

  public MovieRepositoryJdbc(JdbcTemplate jdbcTemplate) {
    this.jdbcTemplate = jdbcTemplate;
  }

  @Override
  public Movie findById(long id) {
    return jdbcTemplate.queryForObject("select * from movies where id = ?", movieMapper, id);
  }

  @Override
  public Collection<Movie> findAll() {

    return jdbcTemplate.query("select * from movies", movieMapper);
  }

  @Override
  public void saveOrUpdate(Movie movie) {
    jdbcTemplate.update("insert into movies (name, minutes, genre) values (?, ?, ?)",
            movie.getName(), movie.getMinutes(), movie.getGenre().toString());
  }

  private static final RowMapper<Movie> movieMapper = (rs, rowNum) ->
    new Movie(
            rs.getInt("id"),
            rs.getString("name"),
            rs.getInt("minutes"),
            Genre.valueOf(rs.getString("genre")));

}
