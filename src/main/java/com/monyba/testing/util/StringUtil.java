package com.monyba.testing.util;

import java.util.stream.IntStream;

public class StringUtil {
    public static String repeat(String str, Integer times){
        if (times<0) throw new IllegalArgumentException("Negative times not allowed");
        return IntStream.range(0,times).mapToObj(e->String.valueOf(e)).reduce("",(t,e)->t+str);
    }

    public static boolean isEmpty(String str) {
        return str == null || str.trim().isEmpty();
    }
}
