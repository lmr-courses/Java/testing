package com.monyba.testing.discounts;

import java.util.ArrayList;
import java.util.List;

public class PriceCalculator {
    private List<Double> prices;
    private Integer discountInPercent;

    public PriceCalculator() {
        this.prices = new ArrayList<>();
        this.discountInPercent = 0;
    }

    public double getTotal() {
        double total = prices.stream().reduce(0.0,Double::sum);
        double discount = total*discountInPercent/100;
        return total - discount;
    }

    public void addPrice(double price) {
        prices.add(price);
    }

    public void setDiscountInPercent(int discountInPercent) {
        this.discountInPercent = discountInPercent;
    }
}
